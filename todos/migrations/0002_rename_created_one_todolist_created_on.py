# Generated by Django 5.0.1 on 2024-01-29 19:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todolist",
            old_name="created_one",
            new_name="created_on",
        ),
    ]

from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list" : todo_list,
    }
    return render(request, "todos/my_lists.html", context)


def todo_list_detail(request, id):
    todo_list_details = TodoList.objects.get(id=id)
    context = {
        "todo_list_detail": todo_list_details
    }
    return render(request, "todos/details.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form = form.save()
            # return redirect("todo_list_detail", id=(TodoList.objects.last()).id)
            return redirect("todo_list_detail", id=form.pk)

    else:
        form = TodoListForm()

    context = {
        "form" : form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    name = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=name)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.list.id)

    else:
        form = TodoItemForm()

    context = {
        "form" : form,
    }
    return render(request, "todos/item/item_create.html", context)


def todo_item_update(request, id):
    name = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=name)
        if form.is_valid():
            listed = form.save()
            return redirect("todo_list_detail", id=listed.list.id)

    else:
        form = TodoItemForm(instance=name)

    context = {
        "form" : form,
    }
    return render(request, "todos/item/item_edit.html", context)
